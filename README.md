#weight function for selection probabilitites
w <-function(x) 1/x

SD <- function (k) {
  sqrt((1/k -1/k^2)/3)
}
library(gtools) ## needed for rdirichlet only

Size=100  # Number of different initials 
N=50000   # Nunmer of steps in each iteration

K=c(3:10,50,100,150,200,500) #Different number of colors 
L=length(K)  
par(mfrow=c(1,2))
U=list()  # Array of urn configurations 
X=list() # Array of urn proportions

Simul_size=1000
X_N=list() # Proportion of colours at time N (For Asymptotic Normality)

TV= array(NA, dim=c(N,Size,L)) # vectorofTotal Variation Distance

for(c in 1:L){
  k=K[c]
  
  set.seed(206)
  Initials <- rdirichlet(Size,rep(1,k)) # Random initial point
  
  # Replacement matrix
  R = list()
  
  # Replacement matrix
  R[[c]]= diag(k) 
  
  X_N[[c]] = array(NA, dim=c(Simul_size,k))
  
  for(s in 1:Simul_size){
    for(i in 1:Size){
      # Set of colours
      C <- 1:k 
      # Number of balls vector
      U[[c]] =array(NA, dim=c(N,k))
      # initial configuration
      U[[c]][1,] = Initials[i,]%/%.001+rep(1,k)
      # Simulated proportion vector 
      X[[c]] = matrix(NA, nrow =N, ncol=k)
      X[[c]][1,] = U[[c]][1,]/sum(U[[c]][1,]) 
      TV[1, i,c] = sum(abs(X[[c]][1,]-1/k))/2
      
      for(n in 2:N){
        # slecting a color with given weight function
        S<- sample(C, size=1,prob=w(X[[c]][(n-1),])) 
        U[[c]][n,]=U[[c]][(n-1),]+R[[c]][S,] # start with previous config
        X[[c]][n,]=  U[[c]][n,]/sum(U[[c]][n,])
        TV[n, i,c] = sum(abs(X[[c]][n,]-1/k))/2
      }
      X_N[[c]][s,] = X[[c]][N,]
    }
  }
}


par(mfrow=c(1,2))
# Graphs for k =3,4
for(c in 1:2){
  TV_max = apply(TV[,,c],1,function(x) max(x))
  plot(TV_max,type="l",xlab="Time (n)", main =bquote("K=" ~ .(K[c])) ,ylab=expression(E[n](K)))
  abline(h=0)
}

# Normality

par(mfrow=c(2,2))
H=hist((X_N[[c]][,1]-mean(X_N[[c]][,1]))*sqrt(N), breaks=40,plot=FALSE)
H$density = H$counts/sum(H$counts)*20 
plot(H,freq=FALSE,ylim=c(0,15),main="",xlab="Proportion of colour 1")
curve(dnorm(x, mean=0, sd=SD(k)),col="darkblue", lwd=2, add=TRUE, yaxt="n")

qqnorm(X_N[[c]][,1], main =bquote("Q-Q plot for color 1 proportion for K=" ~ .(K[c])))
qqline(X_N[[c]][,1])
